CREATE TABLE tbl_product (
  user_id int(11) AUTO_INCREMENT PRIMARY KEY not null,
  user_type varchar (25) null,
  user_sku varchar (255) null,
  user_name varchar (255) null,
  user_price double (10.2) null,

);

INSERT INTO tbl_product (user_sku, user_name, user_price, user_type)
  VALUES ('$sku','$name','$price','$type');
