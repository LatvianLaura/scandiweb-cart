<style><?php include 'includes/main.css'; ?> </style>
<?php
  include_once 'includes/db.php';
?>

 <!DOCTYPE html>
 <html>
      <head>
           <title>Scandiweb</title>
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
           <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

           <meta name="viewport" content="width=device-width, initial-scale=1">
           <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!--   <link rel="stylesheet" href="css/php_checkbox.css" /> -->
      </head>
      <body>
        <!-- Navigation container -->
        <div class="container">
          <div class="grid-container">
            <div class="grid-item">
              <h2>Product List</h2>
            </div>
            <div class="grid-item">
              <div class="dropdown">
                <button class="dropbtn">Mass Delete Action
                  <i class="fa fa-caret-down"></i>
                </button>
                <div class="dropdown-content">
                  <a href="#">Delete Selected</a>
                  <a href="#">Delete All</a>
                </div>
              </div>
            </div>
            <div class="grid-item">
              <div class="grid-button">
                <button type="submit" name="add_to_cart" value="Add to Cart">Apply</button>
              </div>
            </div>
          </div>
        </div>
        <!-- End of nav.container -->
        <hr>
        <?php
          $query = "SELECT * FROM tbl_product ORDER BY id ASC";
          $result = mysqli_query($connect, $query);
          if(mysqli_num_rows($result) > 0) {

            while($row = mysqli_fetch_array($result))

          {
        ?>
        <div class="col-md-3">
          <form method="post" action="index.php?action=add&id=<?php echo $row["id"]; ?>">
            <div class="box">
          </form>
              <input type="checkbox" name="add_to_cart" value="Add to Cart" value="<?php echo $row["id"]; ?>"/>
              <h4 class="text-info">SKU:<?php echo $row["sku"]; ?></h4>
              <h4 class="text-info"><?php echo $row["name"]; ?></h4>
              <h4 class="text-info"><?php echo $row["size"];  ?></h4>
              <h4 class="text-info"><?php echo $row["weight"];  ?></h4>
              <h4 class="text-info"><?php echo $row["dimentions"];  ?></h4>
              <h4 class="text-danger">€ <?php echo $row["price"]; ?></h4>

              <input type="hidden" name="hidden_sku" value="<?php echo $row["sku"]; ?>" />
              <input type="hidden" name="hidden_name" value="<?php echo $row["name"]; ?>" />
              <input type="hidden" name="hidden_size" value="<?php echo $row["size"]; ?>" />
              <input type="hidden" name="hidden_weight" value="<?php echo $row["weight"]; ?>" />
              <input type="hidden" name="hidden_dimentions" value="<?php echo $row["dimentions"]; ?>" />
              <input type="hidden" name="hidden_price" value="<?php echo $row["price"]; ?>" />
            </div>
        </div>
                <?php
                     }
                }
                ?>
      </body>
 </html>
